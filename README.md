# BioProject2

### How to build
Clone this project, Setup a gradle project and import the project.

### Configure input file
when reading the VCF file, you need to transfer vcf format to vcd.gz format, you can use the following command:
Using these two commands to index the vcf file
```bash
bgzip /path/to/your/file.vcf
tabix -p vcf /path/to/your/file.vcf.gz
```

### Main class
`Start.java`

### Commands for disease Sickle Cell Anemia
```bash
java -Xmx4g -cp BioProject2.jar Start -f <path_to_family_vcf_file> -e <path_to_exon_file> -p <path_to_population_vcf_file> -i FATHER:P,MOTHER:P,DAUGHTER1:P,DAUGHTER2:F,DAUGHTER3:N,SON1:P,SON2:F -n Sickle Cell Anemia -pd 0.00059799
```
### Commands for disease Retinitis Pigmentosa
```bash
java -Xmx4g -cp BioProject2.jar Start -f <family_vcf_file> -e <path_to_exon_file> -p <path_to_population_vcf_file> -i FATHER:H,MOTHER:N,DAUGHTER1:N,DAUGHTER2:H,DAUGHTER3:N,SON1:N,SON2:H -n Retinitis Pigmentosa -pd 0.00028571
```
### Commands for disease Severe Skeletal Dysplasia
```bash
java -Xmx4g -cp BioProject2.jar Start -f <family_vcf_file> -e <path_to_exon_file> -p <path_to_population_vcf_file> -i FATHER:N,MOTHER:N,DAUGHTER1:N,DAUGHTER2:H,DAUGHTER3:N,SON1:H,SON2:N -n Severe Skeletal Dysplasia -pd 0.00024
```
### Commands for disease Spastic Paraplegia
```bash
java -Xmx4g -cp BioProject2.jar Start -f <family_vcf_file> -e <path_to_exon_file> -p <path_to_population_vcf_file> -i FATHER:H,MOTHER:N,DAUGHTER1:H,DAUGHTER2:N,DAUGHTER3:H,SON1:N,SON2:H -n Spastic Paraplegia -pd 0.000018
```

### Disease Inheritance Pattern
```
Cell Anemia: "FATHER:P,MOTHER:P,DAUGHTER1:P,DAUGHTER2:F,DAUGHTER3:N,SON1:P,SON2:F"
Retinitis Pigmentosa: "FATHER:H,MOTHER:N,DAUGHTER1:N,DAUGHTER2:H,DAUGHTER3:N,SON1:N,SON2:H"
Skeletal Dysplasia: "FATHER:N,MOTHER:N,DAUGHTER1:N,DAUGHTER2:H,DAUGHTER3:N,SON1:H,SON2:N"
Spastic Paraplegia: "FATHER:H,MOTHER:N,DAUGHTER1:H,DAUGHTER2:N,DAUGHTER3:H,SON1:N,SON2:H"
```

### Parameters
```
 -e,--exons_file <arg>                An input file contains (amongst many
                                      other things), the start and end of
                                      the gene and of individual exons as
                                      well as its "common" name.
 -f,--family_variant_file <arg>       A input vcf.gz file contains the
                                      variants for the simulated member
                                      family.
 -i,--inheritance_pattern <arg>       A pattern for disease inheritance,
                                      separate individuals with ',' and
                                      separate individuals and disease
                                      type with ':'
                                      F(Full Disease), P(Partial Disease),
                                      H(Have Disease), N(No disease)
                                      eg:
                                      FATHER:P,MOTHER:P,DAUGHTER1:P,DAUGHT
                                      ER2:F,DAUGHTER3:N,SON1:P,SON2:F
 -n,--disease_name <arg>              disease name
 -p,--population_variant_file <arg>   A input vcf.gz file with counts of
                                      known variants from a population
                                      sample.
 -pd,--prevalence_of_disease <arg>    prevalence of disease from published
                                      paper
```
