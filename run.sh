#!/bin/bash

# Check number of arguments
if [ "$#" != 3 ]; 
    then
        # Show error messga and exit
        echo "Illegal number of parameters\n\tUsage: <path_to_family_vcf_file> <path_to_population_vcf_file> <path_to_exon_file>"
        exit 1
fi

# Run Sickle Cell Anemia
java -Xmx4g -cp BioProject2.jar Start -f $1 -e $3 -p $2 -i FATHER:P,MOTHER:P,DAUGHTER1:P,DAUGHTER2:F,DAUGHTER3:N,SON1:P,SON2:F -n "Sickle Cell Anemia" -pd 0.00059799

# Run Retinitis Pigmentosa
java -Xmx4g -cp BioProject2.jar Start -f $1 -e $3 -p $2 -i FATHER:H,MOTHER:N,DAUGHTER1:N,DAUGHTER2:H,DAUGHTER3:N,SON1:N,SON2:H -n "Retinitis Pigmentosa" -pd 0.00028571

# Run Severe Skeletal Dysplasia
java -Xmx4g -cp BioProject2.jar Start -f $1 -e $3 -p $2 -i FATHER:N,MOTHER:N,DAUGHTER1:N,DAUGHTER2:H,DAUGHTER3:N,SON1:H,SON2:N -n "Severe Skeletal Dysplasia" -pd 0.00024

# Run Spastic Paraplegia
java -Xmx4g -cp BioProject2.jar Start -f $1 -e $3 -p $2 -i FATHER:H,MOTHER:N,DAUGHTER1:H,DAUGHTER2:N,DAUGHTER3:H,SON1:N,SON2:H -n "Spastic Paraplegia" -pd 0.000018
