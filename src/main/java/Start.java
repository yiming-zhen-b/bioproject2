import objects.*;

public class Start {

    private static String m_FamilyVariantsFile;
    private static String m_ExonsFile;
    private static String m_PopulationVariantsFile;
    private static String m_InheritancePattern;
    private static String m_DiseaseName;
    private static double m_DiseasePrevalence;

    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();

        // String[] pattern = new String[]{
        //         "FATHER:P,MOTHER:P,DAUGHTER1:P,DAUGHTER2:F,DAUGHTER3:N,SON1:P,SON2:F",
        //         "FATHER:H,MOTHER:N,DAUGHTER1:N,DAUGHTER2:H,DAUGHTER3:N,SON1:N,SON2:H",
        //         "FATHER:N,MOTHER:N,DAUGHTER1:N,DAUGHTER2:H,DAUGHTER3:N,SON1:H,SON2:N",
        //         "FATHER:H,MOTHER:N,DAUGHTER1:H,DAUGHTER2:N,DAUGHTER3:H,SON1:N,SON2:H"
        // };
        //
        // String[] disease = new String[]{
        //         "Sickle Cell Anemia",
        //         "Retinitis Pigmentosa",
        //         "Severe Skeletal Dysplasia",
        //         "Spastic Paraplegia"
        // };
        //
        // String[] pro = new String[]{
        //         "0.00059799",
        //         "0.00028571",
        //         "0.00024",
        //         "0.000018"
        // };
        //
        // for(int i = 0; i < pattern.length; i ++){
        //     args = new String[]{
        //             "-" + Args.FAMILY_VARIANT_OPTION, "VCFdata/family_variants.vcf.gz",
        //             "-" + Args.SAMPLE_EXONS, "VCFdata/sample_exons.txt",
        //             "-" + Args.POPULATION_VARIANT_OPTION, "VCFdata/population_variants.vcf.gz",
        //             "-" + Args.INHERITANCE_PATTERN_OPTION, pattern[0],
        //             "-" + Args.DISEASE_NAME, disease[0],
        //             "-" + Args.PREVALENCE_OF_DISEASE, pro[0]
        //     };

        Args arg = new Args(args);

        m_FamilyVariantsFile = arg.getOptionValueAsString(Args.FAMILY_VARIANT_OPTION);
        m_ExonsFile = arg.getOptionValueAsString(Args.SAMPLE_EXONS);
        m_PopulationVariantsFile = arg.getOptionValueAsString(Args.POPULATION_VARIANT_OPTION);
        m_InheritancePattern = arg.getOptionValueAsString(Args.INHERITANCE_PATTERN_OPTION);
        m_DiseaseName = arg.getOptionValueAsString(Args.DISEASE_NAME);
        m_DiseasePrevalence = arg.getOptionValueAsDouble(Args.PREVALENCE_OF_DISEASE);

        System.out.println(m_InheritancePattern);
        System.out.println(m_DiseaseName);

        start();
        // }
        System.out.println("Time taken: " + ((System.currentTimeMillis() - startTime) / 1e3));
        System.err.println("<<<<<<<<<<<-------------------------------->>>>>>>>>>>>>>>>>>");
    }

    private static void start() {

        // convert the pattern
        System.out.println("Converting pattern " + m_DiseaseName);
        InheritancePattern inPattern = new InheritancePattern(m_InheritancePattern);
        inPattern.printPossiblePatterns(m_DiseaseName);

        // ActualVariants to hold the variants that matches the pattern and inside of exon
        ActualVariants actualVariants = new ActualVariants(inPattern);

        //Create the output folder
        System.out.println("Creating output folder for " + m_DiseaseName);
        OutputController oc = new OutputController(inPattern, m_DiseaseName);

        // Read exon file
        System.out.println("Reading Exon file.");
        Exons exons = new Exons(m_ExonsFile);

        // Read the family vcf file and extract useful information
        System.out.println("Reading Family file.");
        FamilyVariants.getPatternMatchedVariants(m_FamilyVariantsFile, inPattern, exons, actualVariants);
        actualVariants.printVariantCount();

        // Clear exon
        exons.clearExon();
        System.out.println("Exons object removed.");

        // Read the population file and extract useful information
        System.out.println("Reading population file.");
        PopulationVariants.getMatchedVariants(m_PopulationVariantsFile, inPattern, actualVariants, m_DiseasePrevalence);

        // Calculating P(D)
        System.out.println("Calculating P(D) for current disease");
        actualVariants.calculatePDForCurrentDisease(inPattern.getPossiblePatterns(), m_DiseasePrevalence);

        // Calculate P(D|V)
        System.out.println("Calculating P(D|V) and print result to csv file.");
        actualVariants.checkDefaultAN().calculatePDVForEachVariation().sortPossibilityResultByKey().printToCSV(oc);
    }
}
