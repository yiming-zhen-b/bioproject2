package diseasechecker;

import htsjdk.variant.variantcontext.Allele;

import java.util.List;

/**
 * Interface of Disease types
 */
public interface DiseaseCheckerInterface {
    /**
     * Check Partial Disease, if the both side are equal and both side is alt
     *
     * @param reference contains reference
     * @param alleles   List of alleles
     * @return boolean
     */
    boolean check(String reference, List<Allele> alleles);
}