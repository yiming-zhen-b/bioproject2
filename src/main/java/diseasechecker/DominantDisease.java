package diseasechecker;

import htsjdk.variant.variantcontext.Allele;
import util.DiseaseCheckerUtils;

import java.util.List;

public class DominantDisease implements DiseaseCheckerInterface {

    /**
     * Check Partial Disease, check if list of alleles only have one reference
     *
     * @param reference contains reference
     * @param alleles   List of alleles
     * @return boolean
     */
    @Override
    public boolean check(String reference, List<Allele> alleles) {
        return DiseaseCheckerUtils.howManyAlternativesInAlleles(alleles, reference) > 0;
    }
}
