package diseasechecker;

import htsjdk.variant.variantcontext.Allele;
import util.DiseaseCheckerUtils;

import java.util.List;

public class FullDisease implements DiseaseCheckerInterface {

    /**
     * Check Full Disease, check if list of alleles have no reference
     *
     * @param reference contains reference
     * @param alleles   List of alleles
     * @return boolean
     */
    @Override
    public boolean check(String reference, List<Allele> alleles) {
        return DiseaseCheckerUtils.howManyAlternativesInAlleles(alleles, reference) == alleles.size();
    }
}