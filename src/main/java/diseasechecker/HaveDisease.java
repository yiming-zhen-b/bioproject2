package diseasechecker;

import htsjdk.variant.variantcontext.Allele;
import util.DiseaseCheckerUtils;

import java.util.List;

public class HaveDisease implements DiseaseCheckerInterface {

    /**
     * Check Partial Disease, if the count of reference in alleles is not equals to the alleles count
     *
     * @param reference contains reference
     * @param alleles   List of alleles
     * @return boolean
     */
    @Override
    public boolean check(String reference, List<Allele> alleles) {
        return DiseaseCheckerUtils.howManyReferencesInAlleles(alleles, reference) != alleles.size();
    }
}
