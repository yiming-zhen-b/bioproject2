package diseasechecker;

import htsjdk.variant.variantcontext.Allele;
import util.DiseaseCheckerUtils;

import java.util.List;

public class NoDisease implements DiseaseCheckerInterface {

    /**
     * Check Partial Disease, check if all alleles in the list of alleles are reference
     *
     * @param reference contains reference
     * @param alleles   List of alleles
     * @return boolean
     */
    @Override
    public boolean check(String reference, List<Allele> alleles) {
        return DiseaseCheckerUtils.howManyReferencesInAlleles(alleles, reference) == alleles.size();
    }
}
