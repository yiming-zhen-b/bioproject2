package diseasechecker;

import java.util.List;

import util.DiseaseCheckerUtils;
import htsjdk.variant.variantcontext.Allele;

public class NoDiseaseForRecessive implements DiseaseCheckerInterface {

    /**
     * Check Partial Disease, check if list of alleles only have one reference
     *
     * @param reference contains reference
     * @param alleles   List of alleles
     * @return boolean
     */
    @Override
    public boolean check(String reference, List<Allele> alleles) {
        return DiseaseCheckerUtils.howManyAlternativesInAlleles(alleles, reference) != alleles.size();
    }
}
