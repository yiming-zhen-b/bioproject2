package diseasechecker;

import java.util.List;

import util.DiseaseCheckerUtils;
import htsjdk.variant.variantcontext.Allele;

public class PartialDisease implements DiseaseCheckerInterface {

    /**
     * Check Partial Disease, check if list of alleles only have one reference
     *
     * @param reference contains reference
     * @param alleles   List of alleles
     * @return boolean
     */
    @Override
    public boolean check(String reference, List<Allele> alleles) {
        return alleles.size() == 2 && DiseaseCheckerUtils.howManyReferencesInAlleles(alleles, reference) == 1;
    }
}
