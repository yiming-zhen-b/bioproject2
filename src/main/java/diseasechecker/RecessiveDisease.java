package diseasechecker;

import java.util.List;

import util.DiseaseCheckerUtils;
import htsjdk.variant.variantcontext.Allele;

public class RecessiveDisease implements DiseaseCheckerInterface {

    /**
     * Check Full Disease, check if list of alleles have no reference
     *
     * @param reference contains reference
     * @param alleles   List of alleles
     * @return boolean
     */
    @Override
    public boolean check(String reference, List<Allele> alleles) {
        return DiseaseCheckerUtils.howManyAlternativesInAlleles(alleles, reference) == alleles.size();
    }
}