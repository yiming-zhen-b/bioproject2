package objects;

import com.opencsv.CSVWriter;
import htsjdk.variant.variantcontext.VariantContext;
import util.CUtil;
import util.DiseaseCheckerUtils;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * chrom + start + ref + alt as the key in map, and count as the value
 *
 * @author yiming
 *
 */
public class ActualVariants {
  private final Set<String> m_DisCode;

  /** Pattern and ActualVariants */
  private final Map<String, Map<VariantKey, VariantValue>> m_ActualVariantsMap = CUtil.makeMap();

  /** pattern adn number of family who has disease and number of population who has disease */
  private final Map<String, Double> m_PatternAndNumFamilyDiseaseAndNumPopulationDiseaseMap =
      CUtil.makeMap();

  /** probability of disease map */
  private final Map<String, Double> m_PDMap = CUtil.makeMap();

  /** */
  private int m_PopulationAN = 0;

  /**
   * Initialise the m_ActualVariantsMap
   *
   * @param inPattern InheritancePattern
   */
  public ActualVariants(InheritancePattern inPattern) {
    m_DisCode = inPattern.getDisCode();
    for (String pattern : inPattern.getPossiblePatterns().keySet()) {
      m_ActualVariantsMap.put(pattern, CUtil.makeMap());
    }
  }

  /**
   * put a variant
   *
   * @param context A record
   */
  void putVariant(
      String patternName,
      VariantContext context,
      int familyAC,
      int familyAN,
      Map<String, String> aPattern) {
    m_ActualVariantsMap
        .get(patternName)
        .put(
            new VariantKey(context.getContig(), String.valueOf(context.getStart())),
            new VariantValue(
                context.getReference(),
                CUtil.makeSet(context.getAlternateAlleles()),
                context.getID(),
                familyAC,
                familyAN,
                1,
                getNumOfVariants(aPattern, context)));
  }

  /**
   * update Alternate Allele Count and Total Allele Count
   *
   * @param context A record
   * @return void
   */
  void updateVariant(String patternName, VariantContext context, double PD) {
    VariantValue acAn =
        m_ActualVariantsMap
            .get(patternName)
            .get(new VariantKey(context.getContig(), String.valueOf(context.getStart())));

    if (m_PopulationAN == 0) {
      m_PopulationAN = Integer.parseInt(context.getAttribute("AN").toString());
    }

    int ac = Integer.parseInt(context.getAttribute("AC").toString());

    if (acAn != null) {
      acAn.update(
          context.getReference(),
          CUtil.makeSet(context.getAlternateAlleles()),
          context.getID(),
          ac,
          m_PopulationAN,
          (ac * PD));
    }
  }

  /**
   * get number of alternatives
   *
   * @param aPattern current pattern
   * @param context a record
   * @return true or false
   */
  private double getNumOfVariants(Map<String, String> aPattern, VariantContext context) {
    double count = 0.0;
    // get this record's reference allele
    String reference = context.getReference().toString();
    // Iterate the key set (Individual column header)
    for (String individual : aPattern.keySet()) {
      if (m_DisCode.contains(aPattern.get(individual))) {
        count +=
            DiseaseCheckerUtils.howManyAlternativesInAlleles(
                context.getGenotype(individual).getAlleles(), reference);
      }
    }
    return count;
  }

  /** getPopulationNum */
  private double getPopulationNum() {
    return (double) m_PopulationAN / 2;
  }

  /**
   * Update the AN, the record is not matched with population
   *
   * @return ActualVariants
   */
  public ActualVariants checkDefaultAN() {
    for (Map<VariantKey, VariantValue> vKvV : m_ActualVariantsMap.values()) {
      for (VariantValue vv : vKvV.values()) {
        if (vv.getAn() < m_PopulationAN) {
          vv.updateAn(m_PopulationAN);
        }
      }
    }
    return this;
  }

  /**
   * get the size of key in map
   *
   * @return int size of the keys in map
   */
  public int size() {
    return m_ActualVariantsMap.keySet().size();
  }

  /**
   * Calculating the probability of disease
   *
   * @param possiblePatterns possible patterns
   * @param PrevalenceOfDiseaseFromPublishedPaper Prevalence from published paper
   * @return true or false
   */
  public void calculatePDForCurrentDisease(
      Map<String, Map<String, String>> possiblePatterns,
      double PrevalenceOfDiseaseFromPublishedPaper) {
    double PDC = getPopulationNum() * PrevalenceOfDiseaseFromPublishedPaper;
    int numOfFamilyMember =
        possiblePatterns.get(possiblePatterns.entrySet().iterator().next().getKey()).size();
    for (String pattern : possiblePatterns.keySet()) {
      Map<String, String> aPattern = possiblePatterns.get(pattern);
      for (String val : aPattern.values()) {
        // if (!val.contains("N")) {
        if (m_DisCode.contains(val)) {
          m_PatternAndNumFamilyDiseaseAndNumPopulationDiseaseMap.putIfAbsent(pattern, PDC);
          m_PatternAndNumFamilyDiseaseAndNumPopulationDiseaseMap.put(
              pattern, m_PatternAndNumFamilyDiseaseAndNumPopulationDiseaseMap.get(pattern) + 1);
        }
      }
      double result =
          m_PatternAndNumFamilyDiseaseAndNumPopulationDiseaseMap.get(pattern)
              / (numOfFamilyMember + getPopulationNum());
      // System.out.println("count: " +
      // m_PatternAndNumFamilyDiseaseAndNumPopulationDiseaseMap.get(pattern));
      // System.out.println("numOfFamilyMember + getPopulationNum(): " + numOfFamilyMember +
      // getPopulationNum());
      // System.out.println("result: " + result);
      m_PDMap.put(pattern, result);
    }
  }

  /**
   * calculate the possibility of variations in DNA lead to the disease
   *
   * @return ActualVariants
   */
  public ActualVariants calculatePDVForEachVariation() {
    for (String pattern : m_ActualVariantsMap.keySet()) {
      Map<VariantKey, VariantValue> vKvV = m_ActualVariantsMap.get(pattern);
      for (VariantValue vv : vKvV.values()) {
        vv.calculatePVD(
            m_PatternAndNumFamilyDiseaseAndNumPopulationDiseaseMap.get(pattern),
            m_PDMap.get(pattern));
      }
    }
    return this;
  }

  /** Print result to CSV */
  public void printToCSV(OutputController oc) {
    try {
      for (String vKvVKey : m_ActualVariantsMap.keySet()) {
        CSVWriter writer = new CSVWriter(new FileWriter(oc.getOutputPath(vKvVKey)));
        writer.writeNext(
            new String[] {
              "Chromosome",
              "Starts",
              "ID",
              "Reference",
              "Alternative",
              "AC",
              "AN",
              "Found",
              "P(V)",
              "P(D)",
              "P(V|D)",
              "P(D|V)"
            });
        for (VariantKey vk : m_ActualVariantsMap.get(vKvVKey).keySet()) {
          VariantValue vv = m_ActualVariantsMap.get(vKvVKey).get(vk);
          writer.writeNext(
              new String[] {
                vk.getChrom(),
                vk.getStarts(),
                vv.getID(),
                vv.getRef(),
                vv.getAlt(),
                String.valueOf(vv.getAc()),
                String.valueOf(vv.getAn()),
                String.valueOf(vv.getCount()),
                String.valueOf(vv.getPV()),
                String.valueOf(m_PDMap.get(vKvVKey)),
                String.valueOf(vv.getPVD()),
                String.valueOf(vv.getPDV())
              });
        }
        writer.close();
      }
    } catch (IOException ignored) {

    }
  }

  /** print variant count */
  public void printVariantCount() {
    System.out.println("Family variants count: ");
    for (String vKvVKey : m_ActualVariantsMap.keySet()) {
      System.out.println(vKvVKey + ": " + m_ActualVariantsMap.get(vKvVKey).size());
    }
  }

  /**
   * sort result by key
   *
   * @return ActualVariants
   */
  public ActualVariants sortPossibilityResultByKey() {
    m_ActualVariantsMap.replaceAll(
        (k, v) ->
            m_ActualVariantsMap.get(k).entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(
                    Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue,
                        LinkedHashMap::new)));
    return this;
  }

  /**
   * sort result by value
   *
   * @return ActualVariants
   */
  public ActualVariants sortPossibilityResultByValue() {
    m_ActualVariantsMap.replaceAll(
        (k, v) ->
            m_ActualVariantsMap.get(k).entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .collect(
                    Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (e1, e2) -> e1,
                        LinkedHashMap::new)));
    return this;
  }

  /**
   * print Possibility Result
   *
   * @param count Limit how many data will be printed
   */
  public void printPossibilityResult(int count) {
    int c = 0;
    for (String vKvVKey : m_ActualVariantsMap.keySet()) {
      for (VariantKey av : m_ActualVariantsMap.get(vKvVKey).keySet()) {
        System.out.println(
            av
                + ", "
                + m_ActualVariantsMap.get(av)
                + ", possibility: "
                + m_ActualVariantsMap.get(vKvVKey).get(av).getPV());
        c++;
        if (c == count) {
          break;
        }
      }
    }
  }

  /** print Actual Variants */
  public void printActualVariants() {
    for (String vKvVKey : m_ActualVariantsMap.keySet()) {
      for (VariantKey av : m_ActualVariantsMap.get(vKvVKey).keySet()) {
        System.out.println(
            av + ": " + m_ActualVariantsMap.get(av) + ", " + m_ActualVariantsMap.get(av));
      }
    }
  }
}
