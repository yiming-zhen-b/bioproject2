package objects;

import org.apache.commons.cli.*;

public class Args {

    // family variant file command line option
    public final static String FAMILY_VARIANT_OPTION = "f";
    // exons file command line option
    public final static String SAMPLE_EXONS = "e";
    // population variant file command line option
    public final static String POPULATION_VARIANT_OPTION = "p";
    // inheritance pattern command line option
    public final static String INHERITANCE_PATTERN_OPTION = "i";
    // disease name
    public final static String DISEASE_NAME = "n";
    // prevalence of disease from published paper
    public final static String PREVALENCE_OF_DISEASE = "pd";

    private static CommandLine m_Cmd;
    private Options m_Options;

    /**
     * objects.Args constructor, parse the arguments
     *
     * @param arg command line argument
     **/
    public Args(String[] arg) {
        // Holds all the options for the iteration
        String[] m_Opts = new String[]{FAMILY_VARIANT_OPTION, SAMPLE_EXONS, POPULATION_VARIANT_OPTION, INHERITANCE_PATTERN_OPTION, DISEASE_NAME, PREVALENCE_OF_DISEASE};
        m_Options = new Options();
        m_Options.addOption(PREVALENCE_OF_DISEASE, "prevalence_of_disease", true, "prevalence of disease from published paper");
        m_Options.addOption(DISEASE_NAME, "disease_name", true, "disease name");
        m_Options.addOption(FAMILY_VARIANT_OPTION, "family_variant_file", true, "A input vcf.gz file contains the variants for the simulated member family.");
        m_Options.addOption(SAMPLE_EXONS, "exons_file", true, "An input file contains (amongst many other things), the start and end of the gene and of individual exons as well as its \"common\" name.");
        m_Options.addOption(POPULATION_VARIANT_OPTION, "population_variant_file", true, "A input vcf.gz file with counts of known variants from a population sample.");
        m_Options.addOption(INHERITANCE_PATTERN_OPTION, "inheritance_pattern", true, "A pattern for disease inheritance, " +
                "\n\t separate individuals with '" + InheritancePattern.INHERITANCE_PATTERN_SPLIT_DELIMITER + "' and " +
                "\n\t separate individuals and disease type with '" + InheritancePattern.INHERITANCE_TYPE_SPLIT_DELIMITER + "'" +
                "\n\t F(Full Disease), P(Partial Disease), H(Have Disease), N(No disease)" +
                "\n\t eg: FATHER:P,MOTHER:P,DAUGHTER1:P,DAUGHTER2:F,DAUGHTER3:N,SON1:P,SON2:F");

        CommandLineParser parser = new BasicParser();

        try {
            // parse the command line arguments
            m_Cmd = parser.parse(m_Options, arg, true);
            for (String opt : m_Opts) {
                if (!m_Cmd.hasOption(opt) || m_Cmd.getOptionValue(opt) == null) {
                    showErrorMessageAndExit(opt);
                }
            }
            pintCommand();
        } catch (ParseException e) {
            printHelp();
        }
    }

    /**
     * get option value
     *
     * @param opt option
     * @return value of option
     **/
    public double getOptionValueAsDouble(String opt) {
        try {
            // parse the command line arguments
            return Double.parseDouble(m_Cmd.getOptionValue(opt));
        } catch (Exception e) {
            showErrorMessageAndExit(opt);
        }
        return 0.0;
    }

    /**
     * get option value
     *
     * @param opt option
     * @return value of option
     **/
    public String getOptionValueAsString(String opt) {
        try {
            // parse the command line arguments
            return m_Cmd.getOptionValue(opt);
        } catch (Exception e) {
            showErrorMessageAndExit(opt);
        }
        return null;
    }

    /**
     * get option value
     *
     * @param opt option
     * @return String [] of option
     **/
    public String[] getOptionValues(String opt) {
        return m_Cmd.getOptionValues(opt);
    }

    /**
     * Print usage and parameters needed
     **/
    private void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("OpenPatrician", m_Options);
    }

    private void showErrorMessageAndExit(String opt) {
        printHelp();
        System.out.println("Parameter for '" + opt + "' not found. " + m_Options.getOption(opt).getDescription());
        System.exit(0);
    }

    private void pintCommand() {
        String[] m_Opts = new String[]{FAMILY_VARIANT_OPTION, SAMPLE_EXONS, POPULATION_VARIANT_OPTION, INHERITANCE_PATTERN_OPTION, DISEASE_NAME, PREVALENCE_OF_DISEASE};
        StringBuilder sb = new StringBuilder("Commands: ");

        for (String opt : m_Opts) sb.append("-").append(opt).append(" ").append(m_Cmd.getOptionValue(opt)).append(" ");

        System.out.println(sb);
    }
}
