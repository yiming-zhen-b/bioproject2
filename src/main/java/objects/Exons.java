package objects;

import htsjdk.variant.variantcontext.VariantContext;
import util.CUtil;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class Exons {

    // chromosome index in exon file
    private final static int CHROM_INDEX = 2;
    // starts index in exon file
    private final static int STARTS_INDEX = 9;
    // ends index in exon file
    private final static int ENDS_INDEX = 10;
    // starts and ends split delimiter
    private final static String START_END_SPLIT_DELIMITER = ",";
    // line split delimiter
    private final static String LINE_SPLIT_DELIMITER = "\t";

    // Exon map holds the chromosome as key, starts and ends as value
    private Map<String, StartsAndEnds> m_ExonMap = CUtil.makeMap();

    /**
     * Exons constructor
     *
     * @param fileName file name
     **/
    public Exons(String fileName) {
        this.read(fileName);
    }

    public void clearExon() {
        m_ExonMap = null;
        System.gc();
    }

    /**
     * Find if this variant inside of exon
     *
     * @param context variants
     * @return boolean      true or false
     **/
    boolean isExon(VariantContext context) {
        // get the exon starts and ends
        StartsAndEnds aStartsAndEnds = m_ExonMap.get(context.getContig());
        // Check if the the extron has the range for corresponding chromosome, and test if this is an exon.
        return aStartsAndEnds != null && aStartsAndEnds.contains(context.getStart());
    }

    /**
     * Read file
     *
     * @param fileName file name
     **/
    private void read(String fileName) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] va = line.split(LINE_SPLIT_DELIMITER);     // split line by tab "\t"
                String chrom = va[CHROM_INDEX].substring(3);        // get chromosome
                // put new StartsAndEnds if not in map
                m_ExonMap.putIfAbsent(chrom, new StartsAndEnds());
                StartsAndEnds aStartsAnsEnds = m_ExonMap.get(chrom);
                // get start position and end position fo each exon region, and append to the existing starts and ends
                aStartsAnsEnds.appendStartsAndEnds(
                        CUtil.convertStringToIntegerList(va[STARTS_INDEX], START_END_SPLIT_DELIMITER),
                        CUtil.convertStringToIntegerList(va[ENDS_INDEX], START_END_SPLIT_DELIMITER)
                );
            }
            reader.close();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public static class StartsAndEnds {

        List<Integer> m_Starts = CUtil.makeList();
        List<Integer> m_Ends = CUtil.makeList();

        /**
         * append the starts and ends to the existing list
         *
         * @param starts list of start positions
         * @param ends   list of end positions
         */
        private void appendStartsAndEnds(List<Integer> starts, List<Integer> ends) {
            m_Starts.addAll(starts);
            m_Ends.addAll(ends);
        }

        /**
         * Check if this Exon contains the variant of the given value
         *
         * @param pos position of the variant of given value
         */
        private boolean contains(int pos) {
            for (int i = 0; i < m_Starts.size(); i++) {
                if (pos >= m_Starts.get(i) && pos <= m_Ends.get(i)) {
                    return true;
                }
            }
            return false;
        }

        @Override
        public String toString() {
            return "StartsAndEnds{" +
                    "Size=[" +
                    (m_Starts.size() == m_Ends.size()) +
                    ", " + m_Starts.size() +
                    ", " + m_Ends.size() +
                    "]" +
                    ", Starts=" + Arrays.toString(m_Starts.toArray()) +
                    ", Ends=" + Arrays.toString(m_Ends.toArray()) +
                    '}';
        }
    }
}
