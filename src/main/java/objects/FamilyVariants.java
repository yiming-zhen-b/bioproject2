package objects;

import htsjdk.samtools.util.CloseableIterator;
import htsjdk.variant.variantcontext.VariantContext;
import htsjdk.variant.vcf.VCFFileReader;

import java.io.File;
import java.util.Map;

public class FamilyVariants {

    /**
     * Read the family variant file and match with pattern and check if the matched variant is inside of exon
     *
     * @param fileName       A file contains Family Variants
     * @param inPattern      inheritance pattern
     * @param actualVariants contains the actual variants
     */
    public static void getPatternMatchedVariants(String fileName, InheritancePattern inPattern, Exons exons, ActualVariants actualVariants) {
        // Reader to read VCF file
        VCFFileReader reader = new VCFFileReader(new File(fileName));
        CloseableIterator<VariantContext> text = reader.iterator();

        int count = 0;
        while (text.hasNext()) {
            count++;
            VariantContext context = text.next();
            // Check if it matches the pattern and it is inside of exon
            Map<String, Map<String, String>> possiblePatterns = inPattern.getPossiblePatterns();
            for (String patternName : possiblePatterns.keySet()) {
                Map<String, String> aPattern = possiblePatterns.get(patternName);
                if (inPattern.match(aPattern, context) && exons.isExon(context)) {
                    actualVariants.putVariant(patternName, context, inPattern.getNumOfAlts(context), inPattern.getFamilyAN(), aPattern);
                }
            }
        }
        System.out.println("Family records: " + count);

        text.close();
        reader.close();
    }
}
