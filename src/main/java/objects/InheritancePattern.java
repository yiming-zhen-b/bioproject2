package objects;

import diseasechecker.*;
import htsjdk.variant.variantcontext.VariantContext;
import util.CUtil;
import util.DiseaseCheckerUtils;

import java.util.*;

public class InheritancePattern {
    private final Set<String> m_DisCode = CUtil.makeSet(Arrays.asList("F", "D", "R", "H"));

    private final static String[] PATTERN_NAMES = new String[]{"default_pattern", "dominant_pattern", "recessive_pattern"};

    // inheritance pattern split delimiter
    final static String INHERITANCE_PATTERN_SPLIT_DELIMITER = ",";

    // inheritance type split delimiter
    final static String INHERITANCE_TYPE_SPLIT_DELIMITER = ":";

    // Store the individual names (column header)
    private Set<String> m_IndividualNames = CUtil.makeSet();

    // InheritancePatternMap Map<pattern, Map<individual, disease>> eg: Map<default_pattern, Map<SON1, F>>
    private final Map<String, Map<String, String>> m_PossiblePatterns = CUtil.makeMap();

    // m_DiseaseChecker simple = disease checker, eg: SON = PartialDisease
    private final Map<String, DiseaseCheckerInterface> m_DiseaseChecker = CUtil.makeMap();

    /**
     * InheritancePattern constructor
     *
     * @param pattern a String
     */
    public InheritancePattern(String pattern) {
        this.convert(pattern);
        this.getDiseaseTypeMappedWithChecker();
    }

    /**
     * match the record with inheritance pattern
     *
     * @param context a record
     * @return true or false
     */
    boolean match(Map<String, String> patternMap, VariantContext context) {
        // get this record's reference allele
        String reference = context.getReference().toString();
        // Iterate the key set (Individual column header)
        for (String key : patternMap.keySet()) {
            // Check with disease (full disease, partial disease, no disease, have disease)
            DiseaseCheckerInterface dci = m_DiseaseChecker.get(patternMap.get(key));
            if (dci == null || !dci.check(reference, context.getGenotype(key).getAlleles())) {
                return false;
            }
        }
        return true;
    }

    /**
     * get number of alternatives
     *
     * @param context a record
     * @return true or false
     */
    int getNumOfAlts(VariantContext context) {
        int count = 0;
        // get this record's reference allele
        String reference = context.getReference().toString();
        // Iterate the key set (Individual column header)
        for (String key : m_IndividualNames) {
            count += DiseaseCheckerUtils.howManyAlternativesInAlleles(context.getGenotype(key).getAlleles(), reference);
        }
        return count;
    }

    /**
     * This converts the pattern Map<individuals name (column header), disease type>
     *
     * @param pattern a String
     */
    private void convert(String pattern) {
        Map<String, String> patternNameMap = CUtil.makeMap();
        if (pattern.contains(":H")) {
            //Dominant
            patternNameMap.put(PATTERN_NAMES[1], pattern.replaceAll(":H", ":D"));
            //Recessive and No disease for Recessive
            patternNameMap.put(PATTERN_NAMES[2], pattern.replaceAll(":H", ":R").replaceAll(":N", ":NR"));
        } else {
            patternNameMap.put(PATTERN_NAMES[0], pattern);
        }

        for (String patternName : patternNameMap.keySet()) {
            Map<String, String> individualVSDiseaseMap = CUtil.makeMap();
            String[] individuals = patternNameMap.get(patternName).split(INHERITANCE_PATTERN_SPLIT_DELIMITER);
            for (String ind : individuals) {
                String[] simpleAndDisType = ind.split(INHERITANCE_TYPE_SPLIT_DELIMITER);
                individualVSDiseaseMap.put(simpleAndDisType[0].toUpperCase(), simpleAndDisType[1].toUpperCase());
            }
            m_PossiblePatterns.put(patternName, individualVSDiseaseMap);
        }
        m_IndividualNames = m_PossiblePatterns.get(m_PossiblePatterns.entrySet().iterator().next().getKey()).keySet();
    }

    public Map<String, Map<String, String>> getPossiblePatterns() {
        return m_PossiblePatterns;
    }

    public void printPossiblePatterns(String diseaseName) {
        //Print possible patterns
        System.out.println("The following are the possible patterns for " + diseaseName);
        for (String st : m_PossiblePatterns.keySet()) {
            System.out.println(st + ", " + m_PossiblePatterns.get(st).toString());
        }
    }

    Set<String> getDisCode() {
        return m_DisCode;
    }

    /**
     * Get the number of simple for the count of the alleles
     */
    int getFamilyAN() {
        return m_IndividualNames.size() * 2;
    }

    /**
     * create disease type mapped with disease checker.
     */
    private void getDiseaseTypeMappedWithChecker() {
        m_DiseaseChecker.put("P", new PartialDisease());
        m_DiseaseChecker.put("F", new FullDisease());
        m_DiseaseChecker.put("N", new NoDisease());
        m_DiseaseChecker.put("H", new HaveDisease());
        m_DiseaseChecker.put("D", new DominantDisease());
        m_DiseaseChecker.put("R", new RecessiveDisease());
        m_DiseaseChecker.put("NR", new NoDiseaseForRecessive());
    }
}
