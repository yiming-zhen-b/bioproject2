package objects;

import util.CUtil;

import java.io.File;
import java.util.Map;

public class OutputController {
    private final static String OUTPUT_FORMAT = ".csv";
    private final static String OUTPUT_FOLDER = "output/";
    private final Map<String, String> m_OutputPathMap = CUtil.makeMap();

    public OutputController(InheritancePattern inPattern, String diseaseName) {
        diseaseName += "/";
        createOutputFolder(inPattern, diseaseName);
        for (String pattern : inPattern.getPossiblePatterns().keySet()) {
            m_OutputPathMap.put(pattern, OUTPUT_FOLDER + diseaseName + pattern + OUTPUT_FORMAT);
        }
    }

    public String getOutputPath(String pattern) {
        return m_OutputPathMap.get(pattern);
    }

    public static void createOutputFolder(InheritancePattern inPattern, String diseaseName) {
        createFolder(OUTPUT_FOLDER);
        createFolder(OUTPUT_FOLDER + diseaseName);
    }

    private static void createFolder(String name) {
        File dir = new File(name);

        // if the directory does not exist, create it
        if (!dir.exists()) {
            dir.mkdir();
        } else {
            System.out.println(name + " directory exists.");
        }
    }
}
