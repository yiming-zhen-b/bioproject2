package objects;

import htsjdk.samtools.util.CloseableIterator;
import htsjdk.variant.variantcontext.VariantContext;
import htsjdk.variant.vcf.VCFFileReader;

import java.io.File;
import java.util.Map;

public class PopulationVariants {
    /**
     * Read file, and get matched variants
     *
     * @param fileName file name
     * @return List<PopulationVariant>
     **/
    public static void getMatchedVariants(String fileName, InheritancePattern inPattern, ActualVariants actualVariants, double PD) {
        // Reader to read VCF file
        VCFFileReader reader = new VCFFileReader(new File(fileName));
        CloseableIterator<VariantContext> text = reader.iterator();

        // update the current variant
        while (text.hasNext()) {
            VariantContext context = text.next();
            Map<String, Map<String, String>> possiblePatterns = inPattern.getPossiblePatterns();
            for (String patternName : possiblePatterns.keySet()) {
                actualVariants.updateVariant(patternName, context, PD);
            }
        }

        text.close();
        reader.close();
    }
}