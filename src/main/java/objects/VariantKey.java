package objects;

import java.util.Objects;

public class VariantKey implements Comparable<VariantKey> {
    private final String m_Chrom, m_Starts;

    /**
     * object.Variant constructor
     *
     * @param m_Chrom  chromosome number
     * @param m_Starts position
     **/
    VariantKey(String m_Chrom, String m_Starts) {
        this.m_Chrom = m_Chrom;
        this.m_Starts = m_Starts;
    }

    /**
     * Get Chromosome
     */
    public String getChrom() {
        return m_Chrom;
    }

    /**
     * Get Starts
     */
    public String getStarts() {
        return m_Starts;
    }

    /**
     * Override the equals for comparison
     *
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VariantKey that = (VariantKey) o;
        return Objects.equals(m_Chrom, that.m_Chrom) &&
                Objects.equals(m_Starts, that.m_Starts);
    }

    /**
     * Override the hashCode to for the key in map
     *
     * @return int
     */
    @Override
    public int hashCode() {
        return Objects.hash(m_Chrom, m_Starts);
    }

    /**
     * Override the toString to print the ActualVariant
     *
     * @return String
     */
    @Override
    public String toString() {
        return "VariantKey{" +
                "m_Chrom='" + m_Chrom +
                ", m_Starts='" + m_Starts +
                '}';
    }

    /**
     * Override the compareTo for comparison
     * It will first compare chrom, then start and then reference
     *
     * @return int
     */
    @Override
    public int compareTo(VariantKey o) {
        try {
            if (Integer.parseInt(this.m_Chrom) == Integer.parseInt(o.m_Chrom)) {
                return Integer.parseInt(this.m_Starts) - Integer.parseInt(o.m_Starts);
            }
            return Integer.parseInt(this.m_Chrom) - Integer.parseInt(o.m_Chrom);
            // if (this.m_Chrom.compareTo(o.m_Chrom) == 0) {
            //     return this.m_Starts.compareTo(o.m_Starts);
            // } else {
            //     return this.m_Chrom.compareTo(o.m_Chrom);
            // }
        } catch (Exception e) {
            return 0;
        }
    }
}