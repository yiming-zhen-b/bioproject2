package objects;

import htsjdk.variant.variantcontext.Allele;
import util.CUtil;

import java.util.Arrays;
import java.util.Set;

public class VariantValue implements Comparable<VariantValue> {
    private int m_Ac, m_An, m_Count;
    private double m_NumOfVariants, m_PV, m_PVD, m_PDV;
    private final Set<String> m_ID = CUtil.makeSet();
    private final Set<Allele> m_Ref = CUtil.makeSet();
    private final Set<Allele> m_Alt;

    /**
     * AcAndAn constructor
     *
     * @param id    id
     * @param ac    Alternate Allele Count
     * @param an    Total Allele Count
     * @param count number of variant found
     * @param alt   alternative
     **/
    VariantValue(Allele ref, Set<Allele> alt, String id, int ac, int an, int count, double numOfVariants) {
        this.m_Ac = ac;
        this.m_An = an;
        m_Ref.add(ref);
        m_ID.add(id);
        this.m_Alt = alt;
        this.m_Count = count;
        this.m_NumOfVariants = numOfVariants;
    }

    /**
     * update ID, AC, AN, Alternatives, reference
     */
    void update(Allele ref, Set<Allele> alt, String id, int ac, int an, double numOfVariants) {
        m_Ref.add(ref);
        m_Alt.addAll(alt);
        m_ID.add(id);
        this.m_Ac += ac;
        this.m_An += an;
        this.m_Count++;
        this.m_NumOfVariants += numOfVariants;
    }

    /**
     * Calculate the possibility
     */
    void calculatePVD(double NumOfPopulationAndFamilyHaveDis, double PD) {
        m_PV = (double) m_Ac / (double) m_An;
        m_PVD = m_NumOfVariants / (NumOfPopulationAndFamilyHaveDis * 2);
        m_PDV = (m_PVD * PD) / m_PV;
    }

    /**
     * Get ID
     */
    public String getID() {
        return Arrays.toString(m_ID.toArray());
    }

    /**
     * Get AC
     */
    public int getAc() {
        return m_Ac;
    }

    /**
     * Get AN
     */
    public int getAn() {
        return m_An;
    }

    /**
     * Update AN
     */
    public void updateAn(int m_An) {
        this.m_An += m_An;
    }

    /**
     * Get Count
     */
    public int getCount() {
        return m_Count;
    }

    /**
     * Get PV
     */
    public double getPV() {
        return m_PV;
    }

    /**
     * Get PVD
     */
    public double getPVD() {
        return m_PVD;
    }

    /**
     * Get PDV
     */
    public double getPDV() {
        return m_PDV;
    }

    /**
     * Get Reference
     */
    public String getRef() {
        return Arrays.toString(m_Ref.toArray());
    }

    /**
     * Get Alternatives
     */
    public String getAlt() {
        return Arrays.toString(m_Alt.toArray());
    }

    @Override
    public String toString() {
        return "Variant Value{" +
                "m_Ref=" + Arrays.toString(m_Ref.toArray()) +
                ", m_Alt=" + Arrays.toString(m_Alt.toArray()) +
                ", m_Ac=" + m_Ac +
                ", m_An=" + m_An +
                ", m_Count=" + m_Count +
                ", m_PV=" + m_PV +
                '}';
    }

    @Override
    public int compareTo(VariantValue o) {
        return this.m_PV < o.m_PV ? 1 : 0;
    }
}
