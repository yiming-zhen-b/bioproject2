package util;

import java.util.*;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CUtil {
    /**
     * convert String To Integer List
     *
     * @param st    a string to split
     * @param split a split delimiter
     * @return List<Integer>
     */
    public static List<Integer> convertStringToIntegerList(String st, String split) {
        List<Integer> newVal = new ArrayList<>();
        String[] val = st.split(split);
        for (String aString : val) {
            newVal.add(Integer.parseInt(aString));
        }
        return newVal;
    }

    public static int[] convertStringToIntegerArray(String st, String split) {
        List<Integer> newVal = convertStringToIntegerList(st, split);
        return newVal.stream().mapToInt(i -> i).toArray();
    }

    public static final Comparator<String> STRING_AS_INT_COMPARATOR = new Comparator<String>() {
        @Override
        public int compare(String o1, String o2) {
            try {
                int i1 = Integer.parseInt(o1);
                int i2 = Integer.parseInt(o2);
                return Integer.compare(i1, i2);
            } catch (NumberFormatException ex) {
            }

            return o1.compareTo(o2);
        }
    };

    public static <T> List<T> makeList() {
        return new ArrayList<T>();
    }

    public static <T> List<T> makeList(Collection<T> as) {
        return new ArrayList<T>(as);
    }

    public static <T> Set<T> makeSet() {
        return new HashSet<T>();
    }

    public static <T> Set<T> makeSet(Collection<T> as) {
        return new HashSet<T>(as);
    }

    public static <K, V> Map<K, V> makeMap() {
        return new HashMap<K, V>();
    }

    public static <K, V> Map<K, V> makeMap(Map<K, V> as) {
        return new HashMap<K, V>(as);
    }

    @SuppressWarnings("unchecked")
    public static <T> Set<T> asSet(T... a) {
        return new HashSet<T>(Arrays.asList(a));
    }

    public static <T> List<String> toStringList(List<T> as) {
        List<String> strings = makeList();
        for (T a : as) {
            strings.add(a.toString());
        }
        return strings;
    }

    public static <K, V> Map<K, V> subMap(Map<K, V> map, Set<K> keySet) {
        Map<K, V> subMap = makeMap();
        for (K key : keySet) {
            subMap.put(key, map.get(key));
        }
        return subMap;
    }

    public static <K> List<K> maxInt(Map<K, Integer> countMap) {
        List<K> result = makeList();
        int max = Integer.MIN_VALUE;
        for (Entry<K, Integer> entry : countMap.entrySet()) {
            int count = entry.getValue();
            if (count > max) {
                max = count;
                result.clear();
                result.add(entry.getKey());
            } else if (count == max) {
                result.add(entry.getKey());
            }
        }

        return result;
    }

    public static <K> List<K> max(Map<K, Double> countMap) {
        List<K> result = makeList();
        double max = 0.0;
        for (Entry<K, Double> entry : countMap.entrySet()) {
            double count = entry.getValue();
            if (count > max) {
                max = count;
                result.clear();
                result.add(entry.getKey());
            } else if (count == max) {
                result.add(entry.getKey());
            }
        }

        return result;
    }

    public static <K> void addInt(Map<K, Integer> to, Map<K, Integer> from) {
        for (Entry<K, Integer> entry : from.entrySet()) {
            K key = entry.getKey();
            Integer toCount = to.get(key);
            if (toCount == null) {
                toCount = 0;
            }
            to.put(key, toCount + entry.getValue());
        }
    }

    public static <K> void add(Map<K, Double> to, Map<K, Double> from) {
        for (Entry<K, Double> entry : from.entrySet()) {
            K key = entry.getKey();
            Double toCount = to.get(key);
            if (toCount == null) {
                toCount = 0.0;
            }
            to.put(key, toCount + entry.getValue());
        }
    }

    public static <K> void divide(Map<K, Double> as, double d) {
        for (K key : makeSet(as.keySet())) {
            double v = as.get(key);
            as.put(key, v / d);
        }
    }

    private static class Record<K> implements Comparable<Record<K>> {
        K Key;
        Double Value;

        public Record(K key, Double value) {
            Key = key;
            Value = value;
        }

        @Override
        public int compareTo(Record<K> o) {
            return Value.compareTo(o.Value);
        }
    }

    public static <K> List<K> getSortedKeysByValue(final Map<K, Double> as) {
        List<Record<K>> records = makeList();
        for (Entry<K, Double> entry : as.entrySet()) {
            records.add(new Record<K>(entry.getKey(), entry.getValue()));
        }
        Collections.sort(records);

        List<K> sorted = makeList();
        for (Record<K> r : records) {
            sorted.add(r.Key);
        }
        return sorted;
    }

    public static <T> Set<T> setMinus(Set<T> s1, Set<T> s2) {
        Set<T> result = makeSet(s1);
        result.removeAll(s2);
        return result;
    }

    public static <X, Y> List<Set<Y>> extractList(List<X> xs, Function<X, Set<Y>> mapper) {
        List<Set<Y>> result = makeList();
        for (X x : xs) {
            result.add(mapper.apply(x));
        }
        return result;
    }

    @SafeVarargs
    public static <T> Set<T> intersection(Set<T>... as) {
        return intersection(Arrays.asList(as));
    }

    public static <T> Set<T> intersection(List<Set<T>> as) {
        if (as.isEmpty()) return makeSet();

        Set<T> result = CUtil.makeSet(as.get(0));
        for (int i = 1; i < as.size(); i++) {
            result.retainAll(as.get(i));
        }
        return result;
    }

    @SafeVarargs
    public static <T> Set<T> union(Set<T>... as) {
        return union(Arrays.asList(as));
    }

    public static <T> Set<T> union(List<Set<T>> as) {
        Set<T> result = makeSet();
        for (Set<T> a : as) {
            result.addAll(a);
        }
        return result;
    }
}
