package util;

import htsjdk.variant.variantcontext.Allele;

import java.util.List;

public class DiseaseCheckerUtils {

    /**
     * Check if the both side are equal
     *
     * @param alleles   List of alleles
     * @param reference the reference to compare
     * @return int           the count how many reference the alleles list have
     */
    public static int howManyReferencesInAlleles(List<Allele> alleles, String reference) {
        int count = 0;
        for (Allele aAllele : alleles) {
            if (aAllele.toString().equals(reference)) count++;
        }
        return count;
    }

    /**
     * Check if the both side are equal
     *
     * @param alleles   List of alleles
     * @param reference the reference to compare
     * @return int           the count how many reference the alleles list have
     */
    public static int howManyAlternativesInAlleles(List<Allele> alleles, String reference) {
        int count = 0;
        for (Allele aAllele : alleles) {
            if (!aAllele.toString().equals(reference)) count++;
        }
        return count;
    }
}
